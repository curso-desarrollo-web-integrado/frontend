<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <title>Document</title>
</head>
<body>
    <header>
        <a href="#" class="logo"> ALULA SHOP </a>
        <nav class="navbar">
            <a href="index.php">INICIO</a>
            <a href="paginas/tienda.php">CATEGORIAS</a>
            <a href="paginas/nosotros.php">NOSOTROS</a>         
            <a href="paginas/contacto.php">CONTACTENOS</a>            
        </nav>
        <!-- <div class="icons">
            <div class="fas fa-bars" id="menu-btn"></div>
            <a href="paginas/carrito.php"><div class="fas fa-shopping-cart" id="cart-btn"></div>
            <a href="paginas/login2.php"><div class="fas fa-user" id=""></div></a>
        </div> -->


  <div class="icons">
    <div class="fas fa-bars" id="menu-btn"></div>
    <a href="paginas/carrito.php"><div class="fas fa-shopping-cart" id="cart-btn"></div></a>
    
    <?php
    session_start(); // Asegúrate de iniciar la sesión
    if (isset($_SESSION['usuario'])) {
        // El usuario está logueado, mostrar el botón de cierre de sesión
        echo '<a href="llamadas/procesoCerrarSesion.php"><div class="fas fa-sign-out-alt" id="logout-btn"></div></a>';
        $rol = $_SESSION['rol'];
       
        if($rol === '1'){
            echo '<a href="paginas/administrador.php"><div class="fas fa-gear" id="admin-btn"></div></a>';
        }
    } else {
        // El usuario no está logueado, mostrar el icono de inicio de sesión
        echo '<a href="paginas/login2.php"><div class="fas fa-user" id="login-btn"></div></a>';
    }
    ?>
</div>
    </header>

    <script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
</body>
</html>