<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="text/css" href="css/estilos.css">
    <title>Document</title>
</head>
<body>
    <footer>
        <section class="pie">
            <div class="box-container">
                <div class="box">
                    <h3>SOBRE NOSOTROS</h3>
                    <h3>ALULA SHOP</h3>
                    <p>Te invitamos a explorar nuestra colección y descubrir el par de zapatos que elevará tu estilo y comodidad a un nuevo nivel.</p>
                    <div class="share">
                        <a href="#" class="fab fa-facebook-f"></a>
                        <a href="#" class="fab fa-twitter"></a>
                        <a href="#" class="fab fa-instagram"></a>
                        <a href="#" class="fab fa-linkedin"></a>
                    </div>
                </div>
                <div class="box">
                    <h3>Garantías</h3>
                    <a href="#" class="links"> <i class="fas fa-phone"></i> +51 999 999 999 </a>
                    <a href="#" class="links"> <i class="fas fa-envelope"></i> soporte@alula.com </a>
                    <a href="#" class="links"> <i class="fas fa-map-marker-alt"></i> Galería Azul, Agustín Gamarra 840 </a>
                </div>
                <div class="box">
                    <h3>Contáctenos </h3>
                    <p></p>
                </div>
            </div>
            <br><br>
        </section>
    </footer>  
</body>
</html>