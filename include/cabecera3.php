<!DOCTYPE html>
<html lang="en">    
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alula Store</title> 
    <link rel="stylesheet" href="../css/cabecera.css">
  
</head>
<body>
    <header class="header">
        <a href="#" class="logo text-decoration-none"> <i></i> ALULA SHOP </a>
        <nav class="navbar">
            <a class="text-decoration-none" href="../index.php">INICIO</a>
            <a class="text-decoration-none" href="../paginas/tienda.php">CATEGORIAS</a>
            <a class="text-decoration-none" href="../paginas/nosotros.php">NOSOTROS</a>
            <a class="text-decoration-none" href="../paginas/contacto.php">CONTACTENOS</a>            
        </nav>
        <div class="icons">
            <div class="fas fa-bars" id="menu-btn"></div>
            <a href="../paginas/carrito.php"><div class="fas fa-shopping-cart" id="cart-btn"></div></a>
            <a href="../paginas/login2.php"><div class="fas fa-user" id=""></div></a>
        </div>
    </header>


    <script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
<!-- </body>
</html> -->