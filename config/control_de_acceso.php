<?php
function verificar_rol($rol_permitido) {
    //session_start();

    if (isset($_SESSION['rol'])) {
        $rol_usuario = $_SESSION['rol'];

        if ($rol_usuario !== $rol_permitido) {
            // Redirigir a una página de error o denegar el acceso
            header("Location: pagina_de_error.php");
            exit();
        }
    } else {
        // Redirigir a la página de inicio de sesión si el usuario no está autenticado
        header("Location: login2.php");
        exit();
    }
}
?>