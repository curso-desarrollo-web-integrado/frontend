<?php
 //variable conexion
function conectar() {
    $conn= mysqli_connect ("localhost","root","","alulastore"); 
   if(!$conn){
        die("No puede conectarse ".mysqli_error());
    }
    else{
        echo "";
    }
    return $conn; 
}
//validar usuario
function validarUsuario($correo,$dni,$conn){
    $sql = "select * from usuarios where correo='$correo' and dni='$dni'";
    $res = mysqli_query($conn, $sql);
    //echo "<script>console.log('$res');</script>";
    $can = mysqli_num_rows($res);
    return $res;
}
//método para agregar registros
function agregarProducto($idproducto,$nomproducto,$preproducto,$imgproducto,$idcategoria,$conn){
    $sql="insert into producto values('$idproducto','$nomproducto','$preproducto','$imgproducto','$idcategoria')";
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
function comprar($idproducto,$nomproducto,$preproducto,$cantidad,$subtotal,$conn){
    $sql="insert into detalleventa values(null,'$idproducto','$nomproducto','$preproducto','$cantidad','$subtotal')";
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para eliminar registros
function eliminarProducto($idproducto,$conn){
    $sql="delete from producto where idproducto='$idproducto'";    
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para actualizar todos los campos de la tabla
function actualizarTodoProducto($idproducto,$nomproducto,$preproducto,$imgproducto,$idcategoria,$conn){
    $sql="update producto set nomproducto='$nomproducto', preproducto='$preproducto', imgproducto='$imgproducto', idcategoria='$idcategoria' where idproducto='$idproducto'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para actualizar registros sin foto
function actualizarProducto($idproducto,$nomproducto,$preproducto,$idcategoria,$conn){
    $sql="update producto set nomproducto='$nomproducto', preproducto='$preproducto', idcategoria='$idcategoria' where idproducto='$idproducto'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para buscar registro
function buscarProducto($idproducto,$conn){
    $sql="select nomproducto, preproducto, imgproducto, idcategoria from producto where idproducto='$idproducto'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
}
    
//método para listar registros
function listarProducto2($conn){
    $sql="select idproducto, nomproducto, preproducto, imgproducto, idcategoria from producto"; 
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}

//metodo para listar reegistros con nomber categoria
function listarProducto($conn){
    $sql="SELECT p.idproducto, p.nomproducto, p.preproducto, p.imgproducto, c.nomcategoria, pro.nomproveedor
    FROM producto p
    INNER JOIN categoria c ON c.idcategoria = p.idcategoria
    INNER JOIN proveedor pro ON pro.idproveedor = p.idproveedor;"; 
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}

//Tabla categoria 
//método para agreegar registros
function agregarcategoria($idcategoria,$nomcategoria,$conn){
    $sql="insert into categoria values('$idcategoria','$nomcategoria')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

//método para eliminar registros 
function eliminarcategoria($idcategoria,$conn){
    $sql="delete from categoria where idcategoria='$idcategoria'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

//método para actualizar registros sin foto
function actualizarcategoria($idcategoria,$nomcategoria,$conn){
    $sql="update categoria set nomcategoria='$nomcategoria' where idcategoria='$idcategoria'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
//método para buscar registro
function buscarcategoria($idcategoria,$conn){
    $sql="select nomcategoria from categoria where idcategoria='$idcategoria'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
}    
//método para listar registros
function listarcategoria($conn){
    $sql="select idcategoria, nomcategoria from categoria";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
//método para listar usuarios
function listarusuarios($conn){
    $sql="select nombres, apellidos, correo, dni, tipo from usuarios";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
//método para listar proveedor
function listarproveedor($conn){
    $sql="select idproveedor ,nomproveedor, pais, representante from proveedor";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
//método para agregar usuario
function agregarusuario($dni,$nombres,$apellidos,$correo,$tipo,$conn){
    $sql="insert into usuarios values('$dni','$nombres','$apellidos','$correo','$tipo')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
 //método para actualizar usuario
 function actualizarusuario ($dni,$nombres,$apellidos,$corre,$tipo,$conn){
    $sql="update usuarios set nombres='$nombres', apellidos='$apellidos', correo='$correo', tipo='$tipo' where dni='$dni'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
 //método para eliminar usuarios
 function eliminarusuario($dni,$conn){
    $sql="delete from usuarios where dni='$dni'";    
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
//método para agregar proveedor
function agregarproveedor($idproveedor,$nomproveedor,$pais,$representante,$conn){
    $sql="insert into proveedor values('$idproveedor','$nomproveedor','$pais','$representante')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
 //método para actualizar proveedor
 function actualizarproveedor ($idproveedor,$nomproveedor,$pais,$representante,$conn){
    $sql="update proveedor set nomproveedor='$nomproveedor', pais='$pais', representante='$representante' where idproveedor='$idproveedor'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
 //método para eliminar proveedor
 function eliminarproveedor($idproveedor,$conn){
    $sql="delete from proveedor where idproveedor='$idproveedor'";    
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
?>