<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/x-icon" href="/assets/logo-vt.svg" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../css/login.css">
    <title>Login Alula</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous"/>
  </head>
  <body class="bg-secondary bg-opacity-10 d-flex justify-content-center align-items-center vh-100">
  <form class="formproducto" action="../llamadas/procesoLogueo.php" method="post">
      <div
        class="bg-white p-5 rounded-5 text-secondary shadow"
        style="width: 25rem">
        <div class="d-flex justify-content-center text-dark fs-1 fw-bold text-center" >
          <a class="text-decoration-none text-dark" href="../index.php" >ALULA SHOP</a></div>
        <div class="text-center fs-3 fw-bold">Login</div>
        <div class="input-group mt-4">
          <div class="input-group-text bg-secondary">
            <img src="../assets/username-icon.svg" alt="username-icon" style="height: 1rem"/>
          </div>
          <input class="form-control bg-lighta" type="text" placeholder="Correo electrónico" name="usuario"/>
        </div>
        <div class="input-group mt-1">
          <div class="input-group-text bg-secondary">
            <img src="../assets/password-icon.svg" alt="password-icon" style="height: 1rem"/>
          </div>
          <input class="form-control bg-light" type="password" placeholder="Contraseña" name="pass"/>
        </div>
        <div>
        <input class="btn btn-secondary text-white w-100 mt-4 fw-semibold shadow-sm" type="submit" name="accion" value="Login">
        </div>
        <div class="d-flex gap-1 justify-content-center mt-1">
          <div>¿No tienes cuenta?</div>
          <a href="../paginas/registrarse.php" class="text-decoration-none text-secondary fw-semibold">Registrate</a>
        </div>
      </div>
  </from>
  </body>
</html>
