<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/carrito.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />
    <link rel="stylesheet" href="../css/styles.css" />
	<link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
            integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
        />
	<title>Mi Carrito</title>
</head>
<body>
	<?php
		session_start();
		include '../include/cabecera3.php';
	?>
	<p class="text-center fs-3 fw-bold">Vista del carrito</p>
	<script src="../js/cabecera.js"></script>
		<div class="container my-1">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<table id="table" class="table table-dark">
								<thead>
									<tr>
										<th>Codigo</th>
										<th>Nombre</th>
										<th>Precio</th>
										<th>Foto</th>
										<th>Categoria</th>
										<th>Sub Total</th>
										<th>Cantidad</th>
										<th colspan="2">Quitar</th>
									</tr>
								</thead>
								<?php
									$total=0;
									if(isset($_SESSION['carrito'])){
										foreach ($_SESSION['carrito'] as $key => $value) {
											$precio=$value['Precio'];
											$foto=$value['foto'];
											$nombre=$value['Nombre'];
											$cantidad=$value['Cantidad']
								?>
									<tr>
										<td name="codigo"><?php echo "$key";?></td>
										<td name="nombre"><?php echo "$nombre"; ?></td>
										<td name="precio"><?php echo " $precio"; ?></td>
										<td name="foto"><?php echo "<img src='../imagenes/$foto' width='100' height='120'>";?></td>
										<? else 				
										?>
										<td name="categoria"><?php $categoria=$value['Categoria']; echo " $categoria"; ?></td>
										<?php
										$subtotal = $value['Precio'] * $value['Cantidad'];
										$total = $total + $subtotal;?>
										<td><?php echo "$subtotal";?> </td>
										<td><?php echo "$cantidad";?></td>
										<td><?php echo "<button class='btn btn-sm btn-danger' onclick=location='carrito.php?codigo=$key'><i class='fa-solid fa-trash-can'></i></button>"; } ?></td>
									</tr>
									<br>	
										<?php		
											if(isset($_REQUEST['eliminar'])){
												session_destroy();
												header("location:carrito.php");
											}
											if(isset($_REQUEST['codigo'])){
												$codi = $_REQUEST['codigo'];
												unset($_SESSION['carrito'][$codi]);
												header("location:carrito.php");
											}
										?>
											<tr>
												<td colspan="6"></td>
												<td colspan="2"><?php echo "Total: S/. $total <br>";}?></td>
											</tr>						
                    </table>
                </div>
            </div>
        </div>		
	<?php
        include "../include/pie.php";
    ?>
</body>
</html>