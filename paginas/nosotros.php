<!DOCTYPE html>
<html lang="en">    
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alula Store</title> 
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="../css/slider.css">
    <link rel="stylesheet" href="../css/nosotros.css">
</head>
<body>


    <?php
        include "../include/cabecera2.php";
    ?>

<main class="nosotros__main">
  <br><br><br><br>
  <h3>SOBRE NOSOTROS</h3>
  <section class="nosotros">
    <div class="box">
      <h2>MISION</h2>
      <p>En nuestra tienda de calzados, nos comprometemos a proporcionar a nuestros clientes una experiencia de compra excepcional al ofrecer una amplia gama de calzado de calidad para damas y caballeros. Nos esforzamos por combinar moda y comodidad en cada par que vendemos, asegurando que nuestros clientes se sientan seguros y elegantes en cada paso que dan.</p>
    </div>
    <div class="imgBx">
      <img src="https://www.calce.es/wp-content/uploads/2019/01/botas-12.jpg" alt="">
    </div>
  </section>
  <section class="images">
    <img src="../imagenes/grupo-de-personas-modelando-zapato-upscaled.png" alt="">
    <img src="../imagenes/hombre-modelando-zapato-upscaled.png" alt="">
  </section>
  <section class="last_content">
    <div class="imgBx">
      <img src="../imagenes/caja-de-zapatos-de-la-marca-alula-upscaled.png" alt="">
    </div>
    <div class="box">
      <h2>VISION</h2>
      <p>Nuestra visión es ser reconocidos como líderes en la industria de calzado de damas y caballeros, ofreciendo una colección diversa que satisface las tendencias actuales y las necesidades individuales de nuestros clientes. Nos esforzamos por expandir nuestro alcance y establecer una presencia sólida tanto en el mercado local como en el internacional. Aspiramos a ser una marca que sea sinónimo de estilo, calidad y servicio excepcional, trabajando continuamente para superar las expectativas de nuestros clientes y mantenernos a la vanguardia de la moda en calzado</p>
    </div>
  </section>
</main>
    


</body>


<?php
        include "../include/pie.php";
    ?>
</html>