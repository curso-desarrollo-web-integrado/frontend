<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="../css/estilobd.css">-->
	
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous"/>
	<title>DataTables.js</title>
        <!-- Bootstrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />
        <!-- DataTable -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" />
        <!-- Font Awesome -->
        <link>
		<link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
            integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
        />
	<title>Bienvenido administrador</title>
</head>

<body>
<?php
		//include "../include/cabecera2.php";
		?>
		<br>
		<section class="cuerpo" id="">
		<?php
		require '../config/conexion.php';
		$conn = conectar();
		session_start();
		if(!isset($_SESSION['usuario']))
			header('location:../paginas/logueo.php'); 
		$nom = $_SESSION['usuario'];
	?>
		<div class="container my-4">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <table id="datatable_users" class="table table-dark">
                        <thead>
							<tr>
								<th>Codigo</th>
								<th>Nombre</th>
								<th colspan="2">Acciones</th>
							</tr>
                        </thead>
						<?php foreach (listarcategoria($conn) as $key => $value){?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td>
					<a class="btneliminar" href="../llamadas/procesocategoria.php?idcategoria=<?=$value[0]?>&accion=Eliminar"><img src="../Imágenes/eliminar.png"></a>
				</td>
				<td>
					<a class="btnmodificar" href="categoria/editarcategoria.php?idcategoria=<?=$value[0]?>"><img src="../Imágenes/modificar.png";></a>
				</td>
			</tr>
		<?php } ?>
                    </table>
                </div>
            </div>
        </div>
		<!-- Bootstrap-->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- DataTable -->
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
        <!-- Custom JS -->
        <script src="main.js"></script>
						</section>
<section class="cuerpo" id="">

		
	<div>
        <input class="btn btn-secondary text-white w-100 mt-4 fw-semibold shadow-sm" type="submit" name="accion" value="Agregar categoria">
		<div>
        <input class="btn btn-secondary text-white w-100 mt-4 fw-semibold shadow-sm" type="submit" name="accion" value="Exportar PDF">
</div></div>
	<center>
		<br>
		<br>
	<h4>Usuario: <?=$nom?></h3>
	<button type="button" class="btnsesion" onclick="location='../llamadas/procesoLogueo.php?accion=Cerrar'"> Cerrar Sesión</button>
	<h2>Mantenimiento de Categoria</h2>
		
	<button type="button" class="btnadd" onclick="location='categoria/agregarcategoria.php'"> Agregar categoria</button>
	<button type="button" class="btnpdf" onclick="location='categoria/agregarcategoria.php'"> Exportar PDF</button><br>
	<!--
	<a href="categoria/agregarcategoria.php">Agregar Categoria</a><br>
	<a href="categoria/listarcategoria.php">Listar Categoria</a><br>-->

	<table>
		<thead>
		<tr>
			<th>Codigo</th>
			<th>Nombre</th>
			<th colspan="2">Acciones</th>
		</tr>
		</thead>
		<?php foreach (listarcategoria($conn) as $key => $value){?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td>
					<a class="btneliminar" href="../llamadas/procesocategoria.php?idcategoria=<?=$value[0]?>&accion=Eliminar"><img src="../Imágenes/eliminar.png"></a>
				</td>
				<td>
					<a class="btnmodificar" href="categoria/editarcategoria.php?idcategoria=<?=$value[0]?>"><img src="../Imágenes/modificar.png";></a>
				</td>
			</tr>
		<?php } ?>
	</table>	
	<h2>Mantenimiento de Producto</h2>
	<button type="button" class="btnadd" onclick="location='producto/agregarProducto.php'"> Agregar producto</button>
	<button type="button" class="btnpdf" onclick="location='producto/agregarProducto.php'"> Exportar PDF</button><br>
	<table>
		<thead>
		<tr>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Precio</th>
			<th>Foto</th>
			<th>Categoria</th>

			<th colspan="2">Acciones</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach (listarProducto($conn) as $key => $value){?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td><?=$value[2]?></td>
				<td><img src="../Imágenes/<?php echo $value[3]?>"width="50" 
				heigth="70";></td>
				<td>aaa<?=$value[4]?></td>
				<td>
					<a class="btneliminar" href="../llamadas/procesoProducto.php?codigo=<?=$value[0]?>&accion=Eliminar"><img src="../Imágenes/eliminar.png" onclick="return confirm('¿Realmente desea eliminar el producto: <?=$value[0]?>?')"></a>
				</td>
				<td>
					<a class="btnmodificar" href="producto/editarProducto.php?codigo=<?php echo $value[0]?>"><img src="../Imágenes/modificar.png";></a>
				</td>
			</tr>
		<?php } ?>
	</table>
	</center>
		</selection>
</body>
</html>