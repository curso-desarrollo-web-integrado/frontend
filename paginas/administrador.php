<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Bienvenido Administrador</title>
        <!-- Bootstrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />
        <link rel="stylesheet" href="../css/styles.css" />
		<link rel="stylesheet" type="text/css" href="../css/estilobd.css">	
		<link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
            integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
        />
    </head>
    <body>

		<br>
		<section class="cuerpo" id="">
		<?php
		require '../config/conexion.php';
		session_start();
		$conn = conectar();
		//session_start();
		if(!isset($_SESSION['usuario']))
			header('location:../paginas/login2.php'); 
		$nom = $_SESSION['usuario'];
	?>

	<header class="header">
			<a href="../index.php" class="logo"> <i></i> ALULA SHOP </a>

			<div class="icons">
				<!-- <div class="fas fa-bars" id="menu-btn"></div>
				<a href="../paginas/carrito.php"><div class="fas fa-shopping-cart" id="cart-btn"></div></a>
				<a href="../paginas/login2.php"><div class="fas fa-user" id=""></div></a> -->

				<?php
				//include "../include/cabecera2.php";

				include '../config/control_de_acceso.php';
				
				if (isset($_SESSION['usuario'])) {
					$rol = $_SESSION['usuario'];
					echo 'usuario: ' . $rol;
					echo '<a href="../llamadas/procesoCerrarSesion.php"><div class="fas fa-sign-out-alt" id="logout-btn"></div></a>';
				} else {
					echo 'No se ha iniciado sesión.';
				}
					verificar_rol('1');
				?>
			</div>
		</header>




		<p class="text-center fs-3 fw-bold">Lista de categoria</p>
        <div class="container my-1">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <table id="table" class="table table-dark">
                        <thead>
                            <tr>
								<th>Codigo</th>
								<th>Nombre</th>
								<th colspan="2">Acciones</th>
							</tr>
                        </thead>
						<?php foreach (listarcategoria($conn) as $key => $value){?>
							<tr>
								<td><?=$value[0]?></td>
								<td><?=$value[1]?></td>
								<td>
									<button class="btn btn-sm btn-primary" onclick="location='categoria/editarcategoria.php?idcategoria=<?=$value[0]?>'"><i class="fa-solid fa-pencil"></i></button>
                        			<button class="btn btn-sm btn-danger" onclick="location='../llamadas/procesocategoria.php?idcategoria=<?=$value[0]?>&accion=Eliminar'"><i class="fa-solid fa-trash-can"></i></button>
								</td>
							</tr>
						<?php
							}
						?>
                    </table>
                </div>
            </div>
        </div>
		</section>
		<section class="cuerpo" id="">
		<p class="text-center fs-3 fw-bold">Lista de productos</p>
		<div class="container my-1">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <table id="table" class="table table-dark">
                        <thead>
                            <tr>
								<th>Id Producto</th>
								<th>Nombre</th>
								<th>Precio</th>
								<th>Imagen</th>
								<th>Categoria</th>
								<th>Proveedor</th>
								<th colspan="2">Acciones</th>
							</tr>
                        </thead>
						<?php foreach (listarProducto($conn) as $key => $value){?>
							<tr>
								<td><?=$value[0]?></td>
								<td><?=$value[1]?></td>
								<td>$ <?=$value[2]?></td>
								<td><img src="../imagenes/<?php echo $value[3]?>"width="50" heigth="70";></td>
								<td><?=$value[4]?></td>
								<td><?=$value[5]?></td>
								<td>
									<button class="btn btn-sm btn-primary" onclick="location='categoria/editarcategoria.php?idcategoria=<?=$value[0]?>'"><i class="fa-solid fa-pencil"></i></button>
                        			<button class="btn btn-sm btn-danger" onclick="location='../llamadas/procesocategoria.php?idcategoria=<?=$value[0]?>&accion=Eliminar'"><i class="fa-solid fa-trash-can"></i></button>
								</td>
							</tr>
						<?php
							}
						?>
                    </table>
                </div>
            </div>
        </div>
		</section>
		</section>
		<section class="cuerpo" id="">
		<p class="text-center fs-3 fw-bold">Lista de usuarios</p>
		<div class="container my-1">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <table id="table" class="table table-dark">
                        <thead>
                            <tr>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Correo</th>
								<th>DNI</th>
								<th>Tipo</th>
								<th colspan="2">Acciones</th>
							</tr>
                        </thead>
						<?php foreach (listarusuarios($conn) as $key => $value){?>
							<tr>
								<td><?=$value[0]?></td>
								<td><?=$value[1]?></td>
								<td><?=$value[2]?></td>
								<td><?=$value[3]?></td>
								<td><?=$value[4]?></td>
								<td>
									<button class="btn btn-sm btn-primary" onclick="location='usuarios/editarusuario.php?dni=<?=$value[3]?>'"><i class="fa-solid fa-pencil"></i></button>
                        			<button class="btn btn-sm btn-danger" onclick="location='../llamadas/procesoUsuario.php?dni=<?=$value[3]?>&accion=Eliminar'"><i class="fa-solid fa-trash-can"></i></button>
								</td>
							</tr>
						<?php
							}
						?>
                    </table>
                </div>
            </div>
        </div>
		</section>
		<section class="cuerpo" id="">
		<p class="text-center fs-3 fw-bold">Lista de proveedores</p>
		<div class="container my-1">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <table id="table" class="table table-dark">
                        <thead>
                            <tr>
								<th>Idproveedor</th>
								<th>Nombre</th>
								<th>Pais</th>
								<th>Representante</th>
								<th colspan="2">Acciones</th>
							</tr>
                        </thead>
						<?php foreach (listarproveedor($conn) as $key => $value){?>
							<tr>
								<td><?=$value[0]?></td>
								<td><?=$value[1]?></td>
								<td><?=$value[2]?></td>
								<td><?=$value[3]?></td>
								<td>
									<button class="btn btn-sm btn-primary" onclick="location='proveedor/editarproveedor.php?idproveedor=<?=$value[0]?>'"><i class="fa-solid fa-pencil"></i></button>
                        			<button class="btn btn-sm btn-danger" onclick="location='../llamadas/procesoproveedor.php?idproveedor=<?=$value[0]?>&accion=Eliminar'"><i class="fa-solid fa-trash-can"></i></button>
								</td>
							</tr>
						<?php
							}
						?>
                    </table>
                </div>
            </div>
        </div>
		</section>
    </body>
</html>