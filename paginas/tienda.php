<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css"
	integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous" />	
	<link rel="stylesheet" type="text/css" href="../css/tienda-test.css">
	<link rel="stylesheet" href="../css/slider.css">
	<script src="js/tienda.js"></script>
	<!--ipt type="text/javascript" src="js/index.js"></script>-->
	<title></title>
</head>
<body>
	<?php
	session_start();
		include "../include/cabecera3.php";
		require '../config/conexion.php';
		$conn = conectar();
		if(!isset($_SESSION['usuario']))
			header('location:../paginas/login2.php'); 
		$nom = $_SESSION['usuario'];
	?>
    <script src="../js/cabecera.js"></script>
	<div class="container my-9">
	<p class="text-center fs-3 fw-bold">Nueva Temporada</p>
	</div>
	<section class="contenedor">
    <div class="productos container my-1">
	<?php 
		foreach (listarProducto($conn) as $key => $value){
	?>
		<div class="info">
			
			<tr>
			<td><img src="../imagenes/<?=$value[3]?>" width="250" height="220"></td>
			<h2><?=$value[1]?></h2>
			<span class="precio" id="precio">$ <?=$value[2]?></span>
			</tr>
			<div class="fila">
			<form method="post" action="">
				<input class="inputcantidad border" type="number" min="1" max="20" step="1" value="1" size="5" name="cantidad">	
				<input type="hidden" name="codigo" value="<?=$value[0]?>">
				<input type="hidden" name="nombre" value="<?=$value[1]?>">
				<input type="hidden" name="precio" value="<?=$value[2]?>">
				<input type="hidden" name="foto" value="<?=$value[3]?>">
				<input type="hidden" name="Categoria" value="<?=$value[4]?>">
				<button name="agregar" value="Agregar">AGREGAR AL CARRITO</button>
			</form>

			</div>			
		</div>
	<?php 
		} 
		 if(isset($_REQUEST['agregar'])){
		 	$cod = $_REQUEST['codigo'];
			$nom = $_REQUEST['nombre'];
			$pre = $_REQUEST['precio'];
			$fot = $_REQUEST['foto'];
			$tip = $_REQUEST['Categoria'];
			$can = $_REQUEST['cantidad'];

			$_SESSION['carrito'][$cod]['Nombre'] = $nom;
			$_SESSION['carrito'][$cod]['Precio'] = $pre;
			$_SESSION['carrito'][$cod]['foto'] = $fot;
			$_SESSION['carrito'][$cod]['Categoria'] = $tip;

			if(isset($_SESSION['carrito'][$cod]))
				$_SESSION['carrito'][$cod]['Cantidad'] += $can;
			else
				$_SESSION['carrito'][$cod]['Cantidad'] = $can;
		 }
		?>
        
    </section>
</body>
<?php
	include "../include/pie.php";
	?>
</html>	