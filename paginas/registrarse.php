<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/x-icon" href="/assets/logo-vt.svg" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../css/login.css">
    <title>Registro Alula</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous"/>
  </head>
  <body class="bg-secondary bg-opacity-10 d-flex justify-content-center align-items-center vh-100">
  <?php
		require '../config/conexion.php';
		$conn = conectar();
		session_start();
	?>
    <form class="formusuario" action="../llamadas/procesoUsuario.php" method="post">
      <div class="bg-white p-5 rounded-5 text-secondary shadow" style="width: 25rem">
        <!-- <div class="d-flex justify-content-center"><img src="../imagenes/logo.jpg" class="logo" style="height: 7rem" alt="login-icon"/>
        </div>-->
        <div class="d-flex justify-content-center text-dark fs-1 fw-bold text-center" >ALULA SHOP</div>
        <div class="text-center fs-3 fw-bold">Crea tu cuenta</div>

        <div class="input-group mt-4">
          <input class="form-control bg-lighta" type="text" placeholder="Correo electrónico" name="correo" id="correo" required />
        </div>
        <div id="errorCorreo" class="form-text error-message mt-2"></div>
        <div class="input-group mt-1">
          <input class="form-control bg-lighta" type="text" placeholder="Nombres" name="nombres" id="nombres" required />
        </div>
        <div id="errorNombres" class="form-text error-message mt-2"></div>
        <div class="input-group mt-1">
          <input class="form-control bg-lighta" type="text" placeholder="Apellidos" name="apellidos" id="apellidos" required />
        </div>
        <div id="errorApellidos" class="form-text error-message mt-2"></div>
        <div class="input-group mt-1">
          <input class="form-control bg-light" type="number" placeholder="DNI" name="dni" id="dni" required />
        </div>
        <div id="errorDNI" class="form-text error-message mt-2"></div>


        <div>
          <input class="btn btn-secondary text-white w-100 mt-4 fw-semibold shadow-sm" type="submit" name="accion" value="Registrarse" id="registroButton" disabled />
        </div>
        <div class="d-flex gap-1 justify-content-center mt-1">
          <div>¿Tienes una cuenta?</div>
          <a href="../paginas/login2.php" class="text-decoration-none text-secondary fw-semibold">Inicia sesión</a>
        </div>
      </div>
    </form>








  <script>
  // Función para validar el formulario
  function validarFormulario() {
    const correo = document.getElementById('correo').value;
    const nombres = document.getElementById('nombres').value;
    const apellidos = document.getElementById('apellidos').value;
    const dni = document.getElementById('dni').value;
    const botonRegistro = document.getElementById('registroButton');
    const errorCorreo = document.getElementById('errorCorreo');
    const errorNombres = document.getElementById('errorNombres');
    const errorApellidos = document.getElementById('errorApellidos');
    const errorDNI = document.getElementById('errorDNI');

    // Valida el correo
    const correoValido = correo.match(/^[\w\.-]+@[\w\.-]+\.\w+$/);
    // Valida que nombres y apellidos no contengan números
    const nombresValidos = !/\d/.test(nombres);
    const apellidosValidos = !/\d/.test(apellidos);
    // Valida que el DNI tenga exactamente 8 dígitos
    const dniValido = /^\d{8}$/.test(dni);

    // Actualiza los mensajes de error debajo de los campos correspondientes
    errorCorreo.textContent = correoValido ? '' : 'El correo electrónico no es válido';
    errorNombres.textContent = nombresValidos ? '' : 'Los nombres no deben contener números';
    errorApellidos.textContent = apellidosValidos ? '' : 'Los apellidos no deben contener números';
    errorDNI.textContent = dniValido ? '' : 'El DNI debe tener 8 dígitos';

    // Activa el botón de registro si todas las validaciones son verdaderas
    if (correoValido && nombresValidos && apellidosValidos && dniValido) {
      botonRegistro.disabled = false;
    } else {
      botonRegistro.disabled = true;
    }
  }

  // Agrega listeners para validar los campos en tiempo real
  document.getElementById('correo').addEventListener('input', validarFormulario);
  document.getElementById('nombres').addEventListener('input', validarFormulario);
  document.getElementById('apellidos').addEventListener('input', validarFormulario);
  document.getElementById('dni').addEventListener('input', validarFormulario);
</script>

  </body>
</html>
