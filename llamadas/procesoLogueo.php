<?php
require '../config/conexion.php';
$conn = conectar();

$val = $_REQUEST['accion'];

if ($val === "Login") {
    $usu = $_REQUEST['usuario'];
    $pas = $_REQUEST['pass'];

    $res = validarUsuario($usu, $pas, $conn);

    if ($res !== false) {
        $can = mysqli_num_rows($res);

        if ($can > 0) {
            session_start();
            $_SESSION['usuario'] = $usu;

            // Obtén los datos del usuario (en este caso, solo asumo una fila)
            $row = mysqli_fetch_assoc($res);
            $_SESSION['rol'] = $row['tipo'];

            if($_SESSION['rol'] === '1') {
                header('location:../paginas/administrador.php');// paginas/administrador.php
            }else{
                header('location:../index.php'); 
            }

           
        } else {
            echo '<script>alert("Usuario o contraseña no existe");window.history.back();</script>';
        }
    } else {
        // Hubo un error en la consulta, maneja el error apropiadamente.
        echo 'Error en la consulta: ' . mysqli_error($conn);
    }
}

if ($val === "Cerrar") {
    session_start();
    session_destroy();
    header('location:../paginas/logueo.php');
}

if ($val == "Cancelar") {
    header('location:../index.php');
}
?>
