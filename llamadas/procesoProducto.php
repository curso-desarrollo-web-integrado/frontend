<?php
	require '../config/conexion.php';
	$conn = conectar();
	$data = $_REQUEST['accion'];
	$idproducto = $_REQUEST['codigo'];
	$nomproducto = $_REQUEST['nombre'];
	$preproducto = $_REQUEST['precio'];
	$imgproducto = $_REQUEST['foto'];
	$idcategoria = $_POST['idcategoria'];
	if($data==="Agregar"){
		if ($idproducto == null && $nomproducto == null && $preproducto && $imgproducto == null) {
			echo '<script>alert("Ingrese los datos");window.history.back();</script>';
		}
		elseif ($idproducto == null || $idproducto ='') {			
			echo '<script>alert("Ingresa el código");window.history.back();</script>';			 
		}
		elseif ($nomproducto == null || $nomproducto ='') {
			echo '<script>alert("Ingresa el nombre del producto");window.history.back();</script>';	
		}
		elseif ($preproducto == null || $preproducto ='') {
			echo '<script>alert("Ingresa el precio del producto");window.history.back();</script>';
		}
		elseif ($imgproducto == null || $imgproducto =''){
			echo '<script>alert("Registre la foto");window.history.back();</script>';
		}		
		else{
		$idproducto = $_REQUEST['codigo'];
		$nomproducto = $_REQUEST['nombre'];
		$preproducto = $_REQUEST['precio'];
		$imgproducto = $_REQUEST['foto'];
		$idcategoria = $_POST['idcategoria'];
		agregarProducto($idproducto,$nomproducto,$preproducto,$imgproducto,$idcategoria,$conn);
		header('location:../paginas/administrador.php'); 
		}
	}
	if($data==="Eliminar"){
		$idproducto = $_REQUEST['codigo'];
		eliminarProducto($idproducto,$conn);
		echo '<script>alert( "El producto '.$idproducto.' fue eliminado.");window.location.href="../paginas/administrador.php";</script>';
		//header('location:../paginas/administrador.php'); 
	}
	if($data==="Actualizar"){
		if ($imgproducto == null || $imgproducto =''){
			echo '<script>alert("Registre la foto");window.history.back();</script>';
		}		
		else{
			$idproducto = $_REQUEST['codigo'];
			$nomproducto = $_REQUEST['nombre'];
			$preproducto = $_REQUEST['precio'];
			$imgproducto = $_REQUEST['foto'];
			$idcategoria = $_POST['idcategoria'];
			actualizarTodoProducto($idproducto,$nomproducto,$preproducto,$imgproducto,$idcategoria,$conn);
			echo '<script>alert( "El producto '.$idproducto.' fue actualizado.");window.location.href="../paginas/administrador.php";</script>';
			//header('location:../paginas/administrador.php');
		}		 
	}
	if($data==="Cancelar"){
		echo '<script>alert("Función cancelada.");window.location.href="../paginas/administrador.php";</script>';
	}
?>