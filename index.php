<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="css/slider.css">
    <title>Alula Shop</title>
</head>
<body>
    <?php
    include 'include/cabecera.php';
    ?>

    <main class="cuerpo">
        <center>
            <div class="contenedor-C">
                <div class="slider">
                    <div class="slides">
                        <input type="radio" name="radio-btn" id="radio1">
                        <input type="radio" name="radio-btn" id="radio2">
                        <input type="radio" name="radio-btn" id="radio3">
                        <input type="radio" name="radio-btn" id="radio4">
                        <input type="radio" name="radio-btn" id="radio5">
                        <div class="slide first">
                            <img src="imagenes/zapato1.jpg" alt="">
                        </div>
                        <div class="slide">
                            <img src="imagenes/zapato2.jpg" alt="">
                        </div>
                        <div class="slide">
                            <img src="imagenes/valerina1.jpg" alt="">
                        </div>
                        <div class="slide">
                            <img src="imagenes/valerina2.jpg" alt="">
                        </div>
                        <div class="nav-auto">
                            <div class="auto-btn1"></div>
                            <div class="auto-btn2"></div>
                            <div class="auto-btn3"></div>
                            <div class="auto-btn4"></div>                   
                        </div>
                    </div>
                    <div class="nav-manual">
                        <label for="radio1" class="manual-btn"></label>
                        <label for="radio2" class="manual-btn"></label>
                        <label for="radio3" class="manual-btn"></label>
                        <label for="radio4" class="manual-btn"></label>
                    </div>
                </div>
            </div>
        </center>
        <br><br>
        <div>
            <center>
                <h2>DESTACADOS DEL MES</h2>
                <br><br>
                <div class="container-items">
                    <div class="item">
                        <img src="imagenes/mocasines.jpg" class="rounded-circle border"><br><br>
                        <h5>Mocasines</h5>
                        <p class="text-center"><a href="paginas/tienda.php" class="btn btn-success">Ir de compras</a></p>
                    </div>
                    <div class="item">
                        <img src="imagenes/tacon.jpg" class="rounded-circle border"><br><br>
                        <h5>Tacones</h5>
                        <p class="text-center"><a href="paginas/tienda.php" class="btn btn-success">Ir de compras</a></p>
                    </div>
                    <div class="item">
                        <img src="imagenes/botines.jpg" class="rounded-circle border"><br><br>
                        <h5>Botines</h5>
                        <p class="text-center"><a href="paginas/tienda.php" class="btn btn-success">Ir de compras</a></p>
                    </div>
                </div>
            </center>
        </div>
        <br><br>
        <div class="bg-light py-5">
            <center>
                <div class="container my-4">
                    <div class="row py-3">
                        <div class="col-lg-6 m-auto">
                            <h1>Nuestras marcas</h1>
                            <p>Mas de 20 marcas importadas que podras encontrar</p>
                        </div>
                        <center>
                            <div class="contenedor-items">
                                    <div class="items">
                                        <img class=" imagen img-fluid brand-img" src="imagenes/marca1.png" alt="Brand Logo">
                                    </div>
                                    <div class="items">
                                        <img class="imagen img-fluid brand-img" src="imagenes/marca2.png" alt="Brand Logo">
                                    </div>
                                    <div class="items">
                                        <img class="imagen img-fluid brand-img" src="imagenes/marca3.png" alt="Brand Logo">
                                    </div>
                                    <div class="items">
                                        <img class="imagen img-fluid brand-img" src="imagenes/marca4.png" alt="Brand Logo">
                                    </div>
                                    <div class="items">
                                        <img class="imagen img-fluid brand-img" src="imagenes/marca5.png" alt="Brand Logo">
                                    </div>
                            </div>
                        </center>
                    </div>
                </div>
            </center>
        </div>
        <br><br>
        <?php
        include 'include/pie.php';
        ?>
    </main>
        
    <script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
</body>
</html>