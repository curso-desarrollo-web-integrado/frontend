-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-10-2023 a las 03:24:14
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alulastore`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` varchar(10) NOT NULL,
  `nomcategoria` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `nomcategoria`) VALUES
('1', 'zapatos'),
('2', 'Taco alto'),
('3', 'Taco bajos'),
('4', 'Taco Kitten'),
('5', 'Taco Cubano'),
('6', 'Botas'),
('7', 'Mocasines');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `dni` int(8) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `correo` varchar(40) NOT NULL,
  `mensaje` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleventa`
--

CREATE TABLE `detalleventa` (
  `iddeventa` int(11) NOT NULL,
  `idproducto` varchar(15) NOT NULL,
  `nomproducto` varchar(50) NOT NULL,
  `preproducto` double(10,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `subtotal` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` varchar(10) NOT NULL,
  `nomproducto` varchar(50) NOT NULL,
  `preproducto` double(10,2) NOT NULL,
  `imgproducto` varchar(250) NOT NULL,
  `idcategoria` varchar(10) NOT NULL,
  `idproveedor` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproducto`, `nomproducto`, `preproducto`, `imgproducto`, `idcategoria`, `idproveedor`) VALUES
('1', 'Kate', 795.00, 'Kate-ChristianLouboutin.jpg', '2', '1'),
('2', 'Bota de Piel', 1390.00, 'BotadePiel-Gucci.jpg', '6', '3'),
('3', 'Mocasines Saffiano', 740.00, 'MocasinesSaffiano-Prada.jpg', '7', '4'),
('4', 'Mocasines Cepilado', 870.00, 'MocasinesCepilladoH-Prada.jpg', '7', '4'),
('5', 'Salón con Tacón', 750.00, 'SalonconTacon-Gucci.jpg', '3', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idproveedor` int(11) NOT NULL,
  `nomproveedor` varchar(50) NOT NULL,
  `pais` varchar(15) NOT NULL,
  `representante` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idproveedor`, `nomproveedor`, `pais`, `representante`) VALUES
(1, 'Christian Louboutin', 'París', 'Roger ebanista'),
(2, 'Jimmy Choo', 'Italia', 'Diana de Gales'),
(3, 'Gucci', 'Italia', 'Patricia Reggiani'),
(4, 'Prada', 'Italia', 'Mario Prada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `dni` varchar(15) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `correo` varchar(90) NOT NULL,
  `tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dni`, `nombres`, `apellidos`, `correo`, `tipo`) VALUES
('48373608', 'Yufri Javier', 'Soto Machaca', 'yufrisoto@gmail.com', 1),
('72868965', 'Saul', 'Villar Quin', 'u20230503@utp.edu.pe', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idproveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
